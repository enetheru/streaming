#!/bin/bash
#if [ "$1" == "-h" ] || [ "$#" -lt "2" ] || [ "$#" -gt "2" ]
#then
#    echo -e \
#        "usage: $0 dest_ip port" \
#        "\neg: $0 127.0.0.1 1234"
#    exit
#fi

echo -e "
Makesure the client has started listening for a connection with:
c -l -p port | mplayer -benchmark -
-------------------------------------------------------------------------------"

INPUT=":0.0+1080,1120"
FPS=30
MAXRATE=6000
BUFSIZE=$(( $MAXRATE / $FPS ))

#change the terminal window title
mytitle="Streaming x11grab$INPUT to tcp://$1:$2"
echo -e '\033]2;'$mytitle'\007'

ffmpeg -hide_banner -loglevel error -threads 0 \
    -f x11grab -follow_mouse 0 -show_region 0 -region_border 3 \
    -video_size  1920x1080 -r $FPS -i $INPUT \
    -f pulse -i default \
    -c:a libmp3lame \
    -c:v libx264 \
    -profile:v high -preset ultrafast -tune zerolatency -pix_fmt yuv420p \
    -x264opts crf=1:vbv-maxrate=$MAXRATE:vbv-bufsize=$BUFSIZE \
    -f mpegts \
    -metadata service_provider="Enetheru" \
    -metadata service_name="Screencast" \
    - | nc -vl -p 1234 | mplayer -benchmark -

#fix the backspace key
stty sane
stty erase \^H 
