#!/bin/bash
if [ "$1" == "-h" ] || [ "$#" -lt "2" ] || [ "$#" -gt "2" ]
then
    echo -e \
        "usage: $0 dest_ip port" \
        "\neg: $0 127.0.0.1 1234"
    exit
fi
clear

LOGLEVEL=error
INPUT="$DISPLAY+1080,1120"
#INPUT="$DISPLAY"
FPS=30
MAXRATE=6000
BUFSIZE=$(( $MAXRATE / $FPS ))

#change the terminal window title
TITLE="Streaming x11grab $INPUT to tcp://$1:$2"
echo -e '\033]2;'$TITLE'\007'

while [ true ]
do
  echo -e "$TITLE
Client needs to be listening using openbsd netcat and mplayer:
> nc -vlk -p port | mplayer -benchmark -
------------------------------------------------------------------------------"
  ffmpeg -hide_banner -loglevel $LOGLEVEL -threads 0 \
    -f x11grab -follow_mouse 0 -show_region 0 -region_border 1 \
    -video_size 1920x1080 -r $FPS -i $INPUT \
    -c:v libx264 \
    -profile:v high -preset ultrafast -tune zerolatency -pix_fmt yuv420p \
    -x264opts keyint="infinite":crf=1:vbv-maxrate=$MAXRATE:vbv-bufsize=$BUFSIZE \
    -f mpegts \
    -metadata service_provider="Enetheru" \
    -metadata service_name="Screencast" \
    - | nc -v $1 $2
  echo Connection failure, waiting for three seconds before retrying
  sleep 3
  clear
done

#fix the backspace key
stty sane
stty erase \^H 
