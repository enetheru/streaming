#!/bin/bash
if [ "$1" == "-h" ] || [ "$#" -lt "2" ] || [ "$#" -gt "2" ]
then
    echo -e \
        "usage: $0 dest_ip port" \
        "\neg: $0 127.0.0.1 1234"
    exit
fi
clear

LOGLEVEL=error
INPUT=/dev/video0
FPS=30
MAXRATE=1500
BUFSIZE=$(( $MAXRATE / $FPS ))

#set the title of the window
TITLE="Streaming video4linux2 $INPUT to tcp://$1:$2"
echo -e '\033]2;'$TITLE'\007'

while [ true ]
do
  echo -e "$TITLE
Client needs to be listening using openbsd netcat and mplayer:
>nc -vlk -p port | mplayer -benchmark -
------------------------------------------------------------------------------"
  ffmpeg -hide_banner -loglevel $LOGLEVEL -threads 0 \
    -f video4linux2 -r $FPS -framerate $FPS -input_format yuyv422 \
    -video_size 320x240 -i $INPUT \
    -c:v libx264 \
    -profile:v high -preset ultrafast -tune zerolatency -pix_fmt yuv420p \
    -x264opts crf=1:vbv-maxrate=$MAXRATE:vbv-bufsize=$BUFSIZE \
    -f mpegts \
    -metadata service_provider="Enetheru" \
    -metadata service_name="Webcam" \
    - | nc -v $1 $2
  echo Connection failure, waiting for three seconds before retrying
  sleep 3
  clear
done

#fix the backspace key
stty sane
stty erase \^H
