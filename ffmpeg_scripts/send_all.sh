#!/bin/bash
if [ "$1" == "-h" ] || [ "$#" == "0" ]
then
    echo -e \
        "usage: $0 dest_ip" \
        "\nStarts the other udp scripts in this directory" \
        "\neg: $0 127.0.0.1"
    exit
fi

TERMINAL=terminology
$TERMINAL -e "./send_screen.sh $1 1234 && read" &
$TERMINAL -e "./send_webcam.sh $1 1235 && read" &
$TERMINAL -e "./send_audio.sh  $1 1236 && read" &
