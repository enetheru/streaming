#!/bin/bash

echo This script expects openbsd netcat.
TERMINAL=terminology

#Start listening
OPTS="-nomouseinput -noconsolecontrols -nolirc -benchmark"
$TERMINAL -e "nc -vlk -p 1234 | mplayer $OPTS -" &
$TERMINAL -e "nc -vlk -p 1235 | mplayer $OPTS -" &

OPTS="-loglevel info -hide_banner -fflags nobuffer -fast -noborder -sync ext"
$TERMINAL -e "nc -vlk -p 1236 | ffplay $OPTS -" &

echo send packets to `hostname -i`
