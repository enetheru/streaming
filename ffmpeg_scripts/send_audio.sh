#!/bin/bash
if [ "$1" == "-h" ] || [ "$#" -lt "2" ] || [ "$#" -gt "2" ]
then
    echo -e \
        "usage: $0 destination_ip port" \
        "\neg $0 127.0.0.1 1234"
    exit
fi
clear

LOGLEVEL=error

#set the title of the window
TITLE="Streaming jack to udp://$1:$2"
echo -e '\033]2;'$TITLE'\007'

while [ true ]
do
  echo -e "$TITLE
This still hasnt been tuned for low latency :|
Client needs to be listening using openbsd netcat and ffmpeg:
> nc -vlk -p port | ffmpeg -fflags +nobuffer -
------------------------------------------------------------------------------"
  ffmpeg -hide_banner -loglevel $LOGLEVEL \
    -f jack -i "udp://$1:$2" \
    -c:a libopus -b:a 384000 -compression_level 0 -frame_duration 2.5 \
    -application lowdelay \
    -f mpegts -frame_size 10 \
    -metadata service_provider="Enetheru" \
    -metadata service_name="Stereo Audio" \
    - | nc -v $1 $2
  echo Connection failure, waiting for three seconds before retrying
  sleep 3
  clear
done
